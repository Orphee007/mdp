# shebang
# !/usr/bin/env python

import string
import time

mot_de_passe = input("Le mot de passe à trouver: ")

liste_lettres_mp = list(str(mot_de_passe))
liste_caracteres_trouves = []

lettres = string.ascii_letters  # abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
liste_lettres_ascci = list(str(lettres))

nombres = string.digits  # 0123456789
liste_nombres = list(str(nombres))

caracteres_speciaux = string.punctuation  # !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
liste_caracteres_speciaux = list(str(caracteres_speciaux))

# On pourrait ajouter d'autres liste :)
liste_lettres_autorisees = liste_lettres_ascci + liste_nombres + liste_caracteres_speciaux

index_liste_lettre_mp = 0
index_liste_lettres_autorisees = 0
debut = time.time()


def trouve_mot_de_passe():

    # caractères non autorisés donc pas dans liste_lettres_autorisees
    for lettremp in liste_lettres_mp:
        if lettremp not in liste_lettres_autorisees:
            print("le caractère: " + lettremp + " n'est pas autorisé")

    for index_liste_lettre_mp, lettre_mp in enumerate(liste_lettres_mp):

        for index_liste_lettres_autorisees, lettre_autorisee in enumerate(liste_lettres_autorisees):
            if lettre_autorisee == lettre_mp:
                liste_caracteres_trouves.insert(index_liste_lettre_mp, lettre_autorisee)

    mot_de_passe_trouve = "".join(liste_caracteres_trouves)
    print("mot de passe trouvé: " + mot_de_passe_trouve + " en " + str(time.time() - debut) + " secondes")


trouve_mot_de_passe()


